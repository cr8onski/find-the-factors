/**
 * Given a number, find and reveal the prime factorization of that number.
 * Input n, the number to factorize
 * Output prime factors, the prime number ^ exponent * prime number ^ exponent ...
 */

const number = +process.argv[2];
const factors = {};
const print = require('../stringUpAnArray.js').stringUpAnArray;
const findPrimes = require('../sieve/sieve3.js').primes;
const upTo = Math.floor(Math.sqrt(number));
const halfway = Math.floor(number / 2);
console.log(number, halfway);

const primes = findPrimes(halfway);
const length = primes.length;

console.log(`Factorizizing ${print(primes.slice(length-10))}\n`);

function factorThis (num, primeIndex) {
    if (primeIndex % 100 === 0) {
        // console.log(number, num, primeIndex, JSON.stringify(factors));
    }
    if (primeIndex >= length || primes[primeIndex] > halfway || num < 2) {
        return;
    }
    if (num % primes[primeIndex] === 0) {
        if (!factors['' + primes[primeIndex]]) {
            factors['' + primes[primeIndex]] = 1;
        } else {
            factors['' + primes[primeIndex]]++;
        }
        num = num/primes[primeIndex];
        if (primes.includes(num)) {
            if (!factors['' + num]) {
                factors['' + num] = 1;
            } else {
                factors['' + num]++;
            }
            return;
        }
        // factorThis(num/primes[primeIndex], primeIndex);
        factorThis(num, primeIndex);
    } else {
        factorThis(num, ++primeIndex);
    }
}
// console.log('before');
factorThis(number, 0);
// console.log('after');
// console.log(JSON.stringify(factors));
makeItLookPretty();

function makeItLookPretty () {
    let primeString = '',
        factorsString = '';
    const keys = Object.keys(factors);
          len = keys.length;
    let tempArr = [],
        front = [ 1 ],
        back = [ number ];

    if (len > 0) {
        for (let k of keys) {
            tempArr.push(`${k} ^ ${factors[k]}`);
        }
        primeString = tempArr.join('  *  ');
    } else {
        primeString = `${number} ^ 1`;
    }

    for (let i = 2; i <= upTo; i++) {
        if (number % i == 0) {
            front.push(i);
            back.push(number/i);
        }
    }
    // this worked for a while to output all the factors in ascending order
    // back = back.sort((a,b)=>{return a-b});
    // factorsString = [ ... front, ... back ].join(', ');

    // now I want to output them in pairs 
    let factorArr = [];
    for (let i in front) {
        if (i >= back.length) {
            factorArr.push(`\t${spacify(front[i])}  *  ${spacify(front[i])}`);
        } else {
            factorArr.push(`\t${spacify(front[i])}  *  ${spacify(back[i])}`)
        }
    }
    factorsString = factorArr.join('\n');

    console.log(`The given number is: ${number}\nThe prime factorization is: ${primeString}\nThe pairs of factors are: \n${factorsString}\n`);

    // handle right justifying numbers and short strings
    function spacify (str) {
        if (typeof str == 'number') {
            str = '' + str;
        } else if (typeof str != 'string') {
            return false;   // this isn't for non strings except for numbers - mostly just numbers
        }
        const SPACES = 10;
        let limit = SPACES > str.length ? SPACES - str.length : 0;  // if it is too big it just runs over and messes stuff up

        let spaces = Array(limit).fill(' ').join('');
        // console.log(`let's see if there are any${spaces}in this string we just made.`);
        let newstr = spaces + str;

        return newstr;
    }

    // function spaces (num) {

    // }
}
